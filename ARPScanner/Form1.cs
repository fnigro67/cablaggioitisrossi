﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using System.Drawing;

namespace ARPScanner
{
    public partial class Form1 : Form
    {
        public List<Thread> oThreads { get; }
        public Database oDatabase { get; }

        public Form1()
        {
            oThreads = new List<Thread>();
            oDatabase = new Database("localhost", "hosts", "lupascu", "lupascu", 3306);

            InitializeComponent();
        }

        private void Scan_Click(object sender, EventArgs e)
        {
            string sBaseSubnet = bNetwork.Text;
            int iAddressStart = Convert.ToInt32(sAddress.Text);
            int iAddressEnd = Convert.ToInt32(eAddress.Text);
            int iSubnetStart = Convert.ToInt32(sSubnet.Text);
            int iSubnetEnd = iSubnetStart + Convert.ToInt32(subnetCount.Text);
            bool isFileEnabled = IsFileEnabled.Checked;
            bool isConnectionFailed = !oDatabase.checkConnection();
            XmlDocument oDocument = new XmlDocument();
            XmlNode oDocumentHeader = oDocument.CreateXmlDeclaration("1.0", "UTF-16", null);
            XmlNode oHosts = oDocument.CreateElement("hosts");
            Stopwatch oWatch = new Stopwatch();
            ARPHelper oHelper = new ARPHelper(oDocument, oHosts, isFileEnabled, oDatabase);

            oDocument.AppendChild(oDocumentHeader);

            if (isConnectionFailed)
            {
                isFileEnabled = true;
                scanComplete.ForeColor = Color.Orange;
                scanComplete.Text = $"Scan started at {DateTime.Now}, but no DB connection!\nCreating XML file instead";
            }
            else
            {
                scanComplete.ForeColor = Color.DarkGreen;
                scanComplete.Text = $"Scan started at {DateTime.Now}";
            }

            oWatch.Start();

            StartProcess(iSubnetStart, iSubnetEnd, iAddressStart, iAddressEnd, ref oHelper, sBaseSubnet);

            oWatch.Stop();

            int iHostCount;

            if (isFileEnabled)
            {
                iHostCount = SaveToFile(oDocument, oHosts);
            }
            else
            {
                iHostCount = oDatabase.GetRowCount("computers");

            }

            TimeSpan ts = oWatch.Elapsed;
            string elapsedTime = string.Format("{0:00}:{1:00}:{2:00}.{3:00}",
            ts.Hours, ts.Minutes, ts.Seconds,
            ts.Milliseconds / 10);

            scanComplete.ForeColor = Color.DarkGreen;
            scanComplete.Text = $"Scan complete,elapsed time: {elapsedTime}\nTotal hosts: {iHostCount}";

        }
        private void Form1_Load(object sender, EventArgs e)
        {
            IsFileEnabled.Checked = true;
        }

        private void StartProcess(int subnetStart,int subnetEnd,int addressStart,int addressEnd, ref ARPHelper helper,string baseSubnet)
        {
            for (int x = subnetStart; x < subnetEnd; x++)
            {
                for (int i = addressStart; i < addressEnd; i++)
                {
                    helper.EnqueueIpAddress($"{baseSubnet}.{x}.{i}");
                }

                for (int i = 0; i < addressEnd; i++)
                {
                    oThreads.Add(new Thread(helper.GetMAC));
                    oThreads[i].Start();
                }

                for (int i = 0; i < addressEnd; i++)
                {
                    oThreads[i].Join();
                }

                oThreads.RemoveRange(0, addressEnd);
            }
        }

        private int SaveToFile(XmlDocument oDocument,XmlNode oHosts)
        {
            oDocument.AppendChild(oHosts);

            XmlWriterSettings settings = new XmlWriterSettings
            {
                Indent = true
            };

            string filename = $"scan_{DateTime.Now.Ticks}.xml";
            XmlWriter writer = XmlWriter.Create(filename, settings);

            oDocument.Save(writer);
            return oDocument.GetElementsByTagName("host").Count;
        }
    }
    
}



