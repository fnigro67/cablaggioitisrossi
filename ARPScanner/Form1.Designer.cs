﻿namespace ARPScanner
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.Scan = new System.Windows.Forms.Button();
            this.IsFileEnabled = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.bNetwork = new System.Windows.Forms.TextBox();
            this.sAddress = new System.Windows.Forms.TextBox();
            this.sSubnet = new System.Windows.Forms.TextBox();
            this.subnetCount = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.eAddress = new System.Windows.Forms.TextBox();
            this.scanComplete = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Scan
            // 
            this.Scan.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Scan.AutoSize = true;
            this.Scan.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Scan.Location = new System.Drawing.Point(12, 161);
            this.Scan.Name = "Scan";
            this.Scan.Size = new System.Drawing.Size(354, 41);
            this.Scan.TabIndex = 0;
            this.Scan.Text = "Start scan";
            this.Scan.UseVisualStyleBackColor = true;
            this.Scan.Click += new System.EventHandler(this.Scan_Click);
            // 
            // IsFileEnabled
            // 
            this.IsFileEnabled.AutoSize = true;
            this.IsFileEnabled.Location = new System.Drawing.Point(12, 68);
            this.IsFileEnabled.Name = "IsFileEnabled";
            this.IsFileEnabled.Size = new System.Drawing.Size(85, 17);
            this.IsFileEnabled.TabIndex = 1;
            this.IsFileEnabled.Text = "Save to file?";
            this.IsFileEnabled.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.75F);
            this.label2.Location = new System.Drawing.Point(5, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(361, 40);
            this.label2.TabIndex = 3;
            this.label2.Text = "ARP Scanner by Lupi";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bNetwork
            // 
            this.bNetwork.Location = new System.Drawing.Point(187, 66);
            this.bNetwork.Name = "bNetwork";
            this.bNetwork.Size = new System.Drawing.Size(179, 20);
            this.bNetwork.TabIndex = 4;
            // 
            // sAddress
            // 
            this.sAddress.Location = new System.Drawing.Point(93, 135);
            this.sAddress.Name = "sAddress";
            this.sAddress.Size = new System.Drawing.Size(88, 20);
            this.sAddress.TabIndex = 5;
            this.sAddress.Text = "0";
            // 
            // sSubnet
            // 
            this.sSubnet.Location = new System.Drawing.Point(93, 101);
            this.sSubnet.Name = "sSubnet";
            this.sSubnet.Size = new System.Drawing.Size(88, 20);
            this.sSubnet.TabIndex = 6;
            // 
            // subnetCount
            // 
            this.subnetCount.Location = new System.Drawing.Point(266, 101);
            this.subnetCount.Name = "subnetCount";
            this.subnetCount.Size = new System.Drawing.Size(100, 20);
            this.subnetCount.TabIndex = 7;
            this.subnetCount.Text = "1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(115, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Base network";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Starting address";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(187, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Subnet count";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 101);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Starting subnet";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(184, 138);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Ending address";
            // 
            // eAddress
            // 
            this.eAddress.Location = new System.Drawing.Point(266, 135);
            this.eAddress.Name = "eAddress";
            this.eAddress.Size = new System.Drawing.Size(100, 20);
            this.eAddress.TabIndex = 13;
            this.eAddress.Text = "255";
            // 
            // scanComplete
            // 
            this.scanComplete.AutoSize = true;
            this.scanComplete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scanComplete.Location = new System.Drawing.Point(12, 214);
            this.scanComplete.Name = "scanComplete";
            this.scanComplete.Size = new System.Drawing.Size(89, 13);
            this.scanComplete.TabIndex = 14;
            this.scanComplete.Text = "Ready to scan";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 261);
            this.Controls.Add(this.scanComplete);
            this.Controls.Add(this.eAddress);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.subnetCount);
            this.Controls.Add(this.sSubnet);
            this.Controls.Add(this.sAddress);
            this.Controls.Add(this.bNetwork);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.IsFileEnabled);
            this.Controls.Add(this.Scan);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Arp Scanner by Lupi";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Scan;
        private System.Windows.Forms.CheckBox IsFileEnabled;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox bNetwork;
        private System.Windows.Forms.TextBox sAddress;
        private System.Windows.Forms.TextBox sSubnet;
        private System.Windows.Forms.TextBox subnetCount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox eAddress;
        private System.Windows.Forms.Label scanComplete;
    }
}

